import {JetView} from 'webix-jet';
import '../styles/app.css';

class ReaderDetInfo extends JetView {
    config() {
        let editedLibraryBooks = [];

        const setOrderedBooks = () => {
            const orderedReaderBooks = this.$$('orderedBooksTable').serialize();
            const titelArr = [];
            orderedReaderBooks.forEach((book) => {
                titelArr.push(book.title);
            });
            const orderedBooks = titelArr.join(';');
            webix.ajax().post('/api/setOrderedBooks', {id: this.reader.id, orderedBooks})
                .catch(() => {
                    webix.message('Server error. Please try again later.', 'error');
                });
        };

        const setReaderBooks = () => {
            const readersBooksId = [];
            const booksOnReader = this.$$('readerBookTable').serialize();
            booksOnReader.forEach((book) => {
                readersBooksId.push(book.id);
            });
            webix.ajax().post('/api/setReaderBooks', {id: this.reader.id, readersBooksId})
                .catch(() => {
                    webix.message('Server error. Please try again later.', 'error');
                });
        };

        const addRemoveButton = {
            rows: [
                {},
                {
                    view: 'button',
                    localId: 'removeButton',
                    type: 'icon',
                    autowidth: true,
                    icon: 'fas fa-arrow-right fa-2x',
                    click: () => {
                        const bookToLibrary = this.$$('readerBookTable').getSelectedItem();
                        if (bookToLibrary) {
                            const bookInLibrary = this.$$('bookTable').getItem(bookToLibrary.id);
                            bookInLibrary.availableAmount++;
                            this.$$('bookTable').updateItem(bookInLibrary.id, bookInLibrary);
                            editedLibraryBooks.push(bookInLibrary);
                            this.$$('readerBookTable').remove(bookToLibrary.id, bookToLibrary);
                        }
                    }
                },
                {
                    view: 'button',
                    localId: 'addButton',
                    type: 'icon',
                    autowidth: true,
                    icon: 'fas fa-arrow-left fa-2x',
                    click: () => {
                        this.$$('readerBookTable').unselectAll();
                        const bookToReader = this.$$('bookTable').getSelectedItem();
                        if (bookToReader) {
                            const orderedBook = this.$$('orderedBooksTable').find(obj => obj.title === bookToReader.title);
                            const isAlreadyHaveBook = this.$$('readerBookTable').getItem(bookToReader.id);
                            if (isAlreadyHaveBook) {
                                webix.message('Current reader already have this book', 'info');
                                return;
                            }
                            if (bookToReader.availableAmount >= 1) {
                                if (orderedBook[0]) {
                                    this.$$('orderedBooksTable').remove(orderedBook[0].id, orderedBook[0]);
                                }
                                if (!orderedBook[0]) {
                                    bookToReader.availableAmount--;
                                }
                                this.$$('bookTable').updateItem(bookToReader.id, bookToReader);
                                editedLibraryBooks.push(bookToReader);
                                const newReaderBook = Object.assign({}, bookToReader);
                                this.$$('readerBookTable').add(newReaderBook);
                            }
                        }
                    }
                },
                {}
            ]
        };

        const bookTable = {
            view: 'datatable',
            localId: 'bookTable',
            select: true,
            on: {
                onAfterSelect: () => {
                    this.$$('readerBookTable').unselectAll();
                }
            },
            columns: [
                {id: 'title', header: [{content: 'textFilter'}, 'Title'], fillspace: true},
                {id: 'availableAmount', header: 'Available'}
            ]
        };

        const readersBooks = {
            view: 'datatable',
            localId: 'readerBookTable',
            select: true,
            on: {
                onAfterSelect: () => {
                    this.$$('bookTable').unselectAll();
                }
            },
            columns: [
                {id: 'title', header: [{content: 'textFilter'}, 'Title'], fillspace: true}
            ]
        };

        const orderedBooks = {
            view: 'datatable',
            localId: 'orderedBooksTable',
            columns: [
                {id: 'title', header: [{content: 'textFilter'}, 'Title'], fillspace: true},
                {id: 'delCol', header: '', template: '{common.trashIcon()}'}
            ],
            onClick: {
                'wxi-trash': (e, id) => {
                    const orderedBook = this.$$('orderedBooksTable').getItem(id);
                    const bookInLibrary = this.$$('bookTable').find(obj => obj.title === orderedBook.title);
                    bookInLibrary[0].availableAmount++;
                    editedLibraryBooks.push(bookInLibrary[0]);
                    this.$$('bookTable').updateItem(bookInLibrary[0].id, bookInLibrary[0]);
                    this.$$('orderedBooksTable').remove(id);
                    return false;
                }
            }
        };

        const readerDetInfoToolbar = {
            view: 'toolbar',
            cols: [
                {view: 'label', label: 'Ordered books', css: 'readerdetInfoLabels'},
                {view: 'label', label: 'Readers books', css: 'readerdetInfoLabels'},
                {view: 'label', label: 'Available books', css: 'readerdetInfoLabels'}
            ]
        };
        const cancelButton = {
            view: 'button',
            localId: 'cancelButton',
            value: 'Cancel',
            click: () => {
                this.getRoot().hide();
            }
        };
        const editButton = {
            view: 'button',
            localId: 'editButton',
            value: 'Save changes',
            click: () => {
                webix.ajax().post('api/updateManyBooks', {books: editedLibraryBooks})
                    .then(() => {
                        editedLibraryBooks = [];
                    })
                    .catch(() => {
                        webix.message('Server error. Please try again later.', 'error');
                    });
                setReaderBooks();
                setOrderedBooks();
                this.getRoot().hide();
            }
        };

        return {
            view: 'window',
            head: {
                view: 'template',
                localId: 'header',
                template: "<span>Readers details</span><span class='webix_icon wxi-close detInfoClose'></span>",
                onClick: {
                    'wxi-close': () => {
                        this.getRoot().hide();
                    }
                }
            },
            localId: 'readerDetInfo',
            position: 'center',
            modal: true,
            height: 800,
            width: 1200,
            body: {
                rows: [
                    readerDetInfoToolbar,
                    {cols: [
                        orderedBooks,
                        readersBooks,
                        addRemoveButton,
                        bookTable
                    ]},
                    {cols: [
                        editButton,
                        cancelButton
                    ]}
                ]

            }
        };
    }
    showWin(reader) {
        this.$$('readerBookTable').clearAll();
        this.$$('orderedBooksTable').clearAll();
        this.reader = reader;
        webix.ajax().get('/api/getAllBooks')
            .then((res) => {
                const books = res.json();
                this.$$('bookTable').parse(books);
            })
            .catch(() => {
                webix.message('Server error. Please try again later.', 'error');
            });
        webix.ajax().get('/api/getReaderBooks', {id: this.reader.id})
            .then((res) => {
                const books = res.json();
                this.$$('readerBookTable').parse(books);
            })
            .catch(() => {
                webix.message('Server error. Please try again later.', 'error');
            });
        webix.ajax().get('/api/getOrderedBooks', {id: this.reader.id})
            .then((res) => {
                const orderedBooks = [];
                const books = res.text();
                if (books) {
                    const titlesArr = books.split(';');
                    titlesArr.forEach((title) => {
                        const orderedBook = {title};
                        orderedBooks.push(orderedBook);
                    });
                }
                this.$$('orderedBooksTable').parse(orderedBooks);
            })
            .catch(() => {
                webix.message('Server error. Please try again later.', 'error');
            });
        this.getRoot().show();
    }
    hideWin() {
        this.getRoot.hide();
    }
}

export default ReaderDetInfo;

