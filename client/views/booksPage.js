import {JetView} from 'webix-jet';
import '../styles/app.css';
import AddBookWin from './addBookWin';

class BooksPage extends JetView {
    config() {
        const createFormats = (book, format) => {
            if (book[format]) {
                const arr = [];
                const filesArr = book[format].split(';');
                for (let i = 0; i < filesArr.length; i++) {
                    const ext = filesArr[i].substr(filesArr[i].lastIndexOf('.'), filesArr[i].length);
                    const link = `<a href='${filesArr[i]}'>Download ${ext}</a>`;
                    arr.push(link);
                }
                return arr.join('<br>');
            }
            return '';
        };

        const addButton = {
            view: 'button',
            localId: 'addButton',
            value: 'Add book',
            click: () => {
                this.addBookWin.showPopup();
            }
        };
        const editButton = {
            view: 'button',
            localId: 'editButton',
            value: 'Edit book',
            click: () => {
                const item = this.$$('booksTable').getSelectedItem();
                if (item) {
                    this.addBookWin.showPopup(item);
                }
            }
        };
        const delButton = {
            view: 'button',
            localId: 'delButton',
            value: 'Delete book',
            click: () => {
                const item = this.$$('booksTable').getSelectedItem();
                if (item) {
                    this.$$('booksTable').remove(item.id);
                }
            }
        };

        const booksTable = {
            view: 'datatable',
            localId: 'booksTable',
            url: '/api/getAllBooks',
            save: '->/api/changeBooks',
            select: true,
            fixedRowHeight: false,
            rowLineHeight: 25,
            rowHeight: 100,
            scheme: {
                $change: (obj) => {
                    const item = obj;
                    const format = webix.Date.strToDate('%Y.%m.%d');
                    item.year = format(obj.year);
                    item.author = `${item.authorFirstName} ${item.authorMiddleName} ${item.authorSurName}`;
                    return item;
                }
            },
            columns: [
                {id: 'post',
                    header: 'Poster',
                    width: 110,
                    template(obj) {
                        if (obj.poster === null || obj.poster === undefined || obj.poster === '') {
                            return "<img style='width:100px;height:110px' src='./img/bookDefault.png'>";
                        }
                        return `<img style='width:100px;height:110px' src='${obj.poster}'>`;
                    }},
                {id: 'title', header: ['Title', {content: 'textFilter'}], sort: 'text', fillspace: true},
                {id: 'pages', header: ['Pages', {content: 'numberFilter'}], sort: 'int', fillspace: true},
                {id: 'year', header: ['Year', {content: 'dateFilter'}], format: webix.Date.dateToStr('%Y.%m.%d'), sort: 'date', fillspace: true},
                {id: 'author', header: ['Author', {content: 'textFilter'}], sort: 'text', fillspace: true},
                {id: 'publisher', header: ['Publisher', {content: 'textFilter'}], sort: 'text', fillspace: true},
                {id: 'country', header: ['Country', {content: 'textFilter'}], sort: 'text', fillspace: true},
                {id: 'genre', header: ['Genre', {content: 'textFilter'}], sort: 'text', fillspace: true},
                {id: 'availableAmount', header: ['Available', {content: 'numberFilter'}], sort: 'int', fillspace: true},
                {id: 'rating', header: ['Rating', {content: 'numberFilter'}], sort: 'int', fillspace: true},
                {id: 'textFormats',
                    header: 'Text formats',
                    template: obj => createFormats(obj, 'textFormats'),
                    fillspace: true
                },
                {id: 'audioFormats',
                    header: 'Audio formats',
                    template: obj => createFormats(obj, 'audioFormats'),
                    fillspace: true
                }
            ],
            on: {
                onItemDblClick: () => {
                    const item = this.$$('booksTable').getSelectedItem();
                    if (item) {
                        this.addBookWin.showPopup(item);
                    }
                }
            }
        };
        return {
            rows: [
                booksTable,
                {cols: [
                    addButton,
                    editButton,
                    delButton
                ]}
            ]
        };
    }
    init() {
        this.addBookWin = this.ui(AddBookWin);
        this.on(this.app, 'addBook', (values) => {
            this.$$('booksTable').add(values);
        });
        this.on(this.app, 'editBook', (values) => {
            this.$$('booksTable').updateItem(values.id, values);
        });
    }
}

export default BooksPage;
