import {JetView} from 'webix-jet';

class ReaderBookList extends JetView {
    config() {
        let editedLibraryBooks = [];

        const addRemoveButton = {
            rows: [
                {},
                {
                    view: 'button',
                    localId: 'removeButton',
                    type: 'icon',
                    autowidth: true,
                    icon: 'fas fa-arrow-right fa-2x',
                    click: () => {
                        const bookToLibrary = this.$$('readerBookTable').getSelectedItem();
                        if (bookToLibrary) {
                            const bookInLibrary = this.$$('bookTable').find(obj => obj.title === bookToLibrary.title);
                            bookInLibrary[0].availableAmount++;
                            this.$$('bookTable').updateItem(bookInLibrary[0].id, bookInLibrary[0]);
                            editedLibraryBooks.push(bookInLibrary[0]);
                            this.$$('readerBookTable').remove(bookToLibrary.id, bookToLibrary);
                        }
                    }
                },
                {
                    view: 'button',
                    localId: 'addButton',
                    type: 'icon',
                    autowidth: true,
                    icon: 'fas fa-arrow-left fa-2x',
                    click: () => {
                        this.$$('readerBookTable').unselectAll();
                        const bookToReader = this.$$('bookTable').getSelectedItem();
                        if (bookToReader) {
                            const isAlreadyHaveBook = this.$$('readerBookTable').find(obj => obj.title === bookToReader.title);
                            if (isAlreadyHaveBook[0]) {
                                webix.message('You already ordered this book', 'info');
                                return;
                            }
                            if (bookToReader.availableAmount >= 1) {
                                bookToReader.availableAmount--;
                                this.$$('bookTable').updateItem(bookToReader.id, bookToReader);
                                editedLibraryBooks.push(bookToReader);
                                const newReaderBook = Object.assign({}, bookToReader);
                                this.$$('readerBookTable').add(newReaderBook);
                            }
                        }
                    }
                },
                {}
            ]
        };

        const bookTable = {
            view: 'datatable',
            localId: 'bookTable',
            select: true,
            on: {
                onAfterSelect: () => {
                    this.$$('readerBookTable').unselectAll();
                }
            },
            columns: [
                {id: 'title', header: [{content: 'textFilter'}, 'Title'], fillspace: true},
                {id: 'availableAmount', header: 'Available'}
            ]
        };

        const readersBooks = {
            view: 'datatable',
            localId: 'readerBookTable',
            select: true,
            on: {
                onAfterSelect: () => {
                    this.$$('bookTable').unselectAll();
                }
            },
            columns: [
                {id: 'title', header: [{content: 'textFilter'}, 'Title'], fillspace: true}
            ]
        };

        const readerDetInfoToolbar = {
            view: 'toolbar',
            cols: [
                {view: 'label', label: 'Ordered books', css: 'readerdetInfoLabels'},
                {view: 'label', label: 'Available books', css: 'readerdetInfoLabels'}
            ]
        };
        const editButton = {
            view: 'button',
            localId: 'editButton',
            value: 'Save changes',
            click: () => {
                webix.ajax().post('api/updateManyBooks', {books: editedLibraryBooks})
                    .then(() => {
                        editedLibraryBooks = [];
                    })
                    .catch(() => {
                        webix.message('Server error. Please try again later.', 'error');
                    });

                const booksOnReader = this.$$('readerBookTable').serialize();
                const titelArr = [];
                booksOnReader.forEach((book) => {
                    titelArr.push(book.title);
                });
                const orderedBooks = titelArr.join(';');
                webix.ajax().post('/api/setOrderedBooks', {id: this.user.id, orderedBooks})
                    .then(() => {
                        webix.message('Your request succesfully sent to librarian');
                    })
                    .catch(() => {
                        webix.message('Server error. Please try again later.', 'error');
                    });
            }
        };

        return {
            rows: [
                readerDetInfoToolbar,
                {cols: [
                    readersBooks,
                    addRemoveButton,
                    bookTable
                ]},
                {cols: [
                    editButton
                ]}
            ]

        };
    }
    init() {
        this.user = this.app.getService('user').getUser();
        webix.ajax().get('/api/getAllBooks')
            .then((res) => {
                const books = res.json();
                this.$$('bookTable').parse(books);
            })
            .catch(() => {
                webix.message('Server error. Please try again later.', 'error');
            });
        webix.ajax().get('/api/getOrderedBooks', {id: this.user.id})
            .then((res) => {
                const orderedBooks = [];
                const books = res.text();
                if (books) {
                    const titlesArr = books.split(';');
                    titlesArr.forEach((title) => {
                        const orderedBook = {title};
                        orderedBooks.push(orderedBook);
                    });
                }
                this.$$('readerBookTable').parse(orderedBooks);
            })
            .catch(() => {
                webix.message('Server error. Please try again later.', 'error');
            });
    }
}

export default ReaderBookList;
