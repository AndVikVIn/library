import {JetView} from 'webix-jet';

class UserDetInfo extends JetView {
    config() {
        const emailCheck = () => {
            const value = this.$$('email').getValue();
            return webix.ajax().get('/api/checkEmail', {email: value})
                .then((data) => {
                    if (data.text() === '[]') {
                        this.$$('email').check = false;
                    }
                    else {
                        const emailOwnerId = data.json();
                        const editUserId = this.$$('userForm').getValues().id;
                        if (emailOwnerId === editUserId) {
                            this.$$('email').check = false;
                        }
                        else {
                            this.$$('email').check = true;
                        }
                    }
                });
        };
        const getRandomInt = (min, max) => Math.floor((Math.random() * (max - min)) + min);

        const checkReaderCardNumber = value => webix.ajax().get('api/checkCardNumber', {readerCardNumber: value})
            .then((data) => {
                if (data.text() === '[]') {
                    this.$$('readerCardNumber').check = true;
                    return;
                }
                const cardNumberOwnerId = data.json();
                const editUserId = this.$$('userForm').getValues().id;
                if (cardNumberOwnerId === editUserId) {
                    this.$$('readerCardNumber').check = true;
                }
                else {
                    this.$$('readerCardNumber').check = false;
                }
            });

        let phoneAmount = 0;
        const hideLine = (num) => {
            this.$$(`phoneLine${num}`).hide();
            this.$$(`phone${num}`).setValue('');
            phoneAmount--;
        };
        const phonesLines = {
            localId: 'phonesLines',
            rows: [
                {localId: 'phoneLine0',
                    cols: [
                        {view: 'text', label: 'Phone', localId: 'phone0', name: 'phone0', pattern: {mask: '###-## #######', allow: /[0-9]/g}, placeholder: '375-## #######'}
                    ]},
                {localId: 'phoneLine1',
                    hidden: true,
                    cols: [
                        {view: 'text', label: 'Phone', localId: 'phone1', name: 'phone1', pattern: {mask: '###-## #######', allow: /[0-9]/g}, placeholder: '375-## #######'},
                        {template: '<i class="fas fa-times fa-2x"></i>', borderless: true, width: 30, onClick: {'fa-times': () => { hideLine(1); }}}
                    ]},
                {localId: 'phoneLine2',
                    hidden: true,
                    cols: [
                        {view: 'text', label: 'Phone', localId: 'phone2', name: 'phone2', pattern: {mask: '###-## #######', allow: /[0-9]/g}, placeholder: '375-## #######'},
                        {template: '<i class="fas fa-times fa-2x"></i>', borderless: true, width: 30, onClick: {'fa-times': () => { hideLine(2); }}}
                    ]},
                {localId: 'phoneLine3',
                    hidden: true,
                    cols: [
                        {view: 'text', label: 'Phone', localId: 'phone3', name: 'phone3', pattern: {mask: '###-## #######', allow: /[0-9]/g}, placeholder: '375-## #######'},
                        {template: '<i class="fas fa-times fa-2x"></i>', borderless: true, width: 30, onClick: {'fa-times': () => { hideLine(3); }}}
                    ]}
            ]};
        return {
            view: 'popup',
            localId: 'userDetInfo',
            position: 'center',
            height: 800,
            width: 800,
            modal: true,
            body: {
                view: 'form',
                localId: 'userForm',
                elementsConfig: {
                    labelWidth: 200,
                    labelAlign: 'left'
                },
                elements: [
                    {type: 'header', template: 'User detail info'},
                    {view: 'select',
                        label: 'Role',
                        name: 'role',
                        options: [
                            {id: 'admin', value: 'Admin'},
                            {id: 'librarian', value: 'Librarian'},
                            {id: 'reader', value: 'Reader'}
                        ]},
                    {view: 'text', label: 'First name', attributes: {maxlength: 20}, name: 'firstName', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                    {view: 'text', label: 'Middle name', attributes: {maxlength: 20}, name: 'middleName', invalidMessage: "Can't be empty"},
                    {view: 'text', label: 'Last name', attributes: {maxlength: 20}, name: 'lastName', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                    {view: 'text', label: 'Passport #', attributes: {maxlength: 20}, name: 'passportNumber'},
                    {view: 'text', localId: 'readerCardNumber', label: 'Reader card number', attributes: {maxlength: 20}, name: 'readerCardNumber', invalidMessage: 'Should be unique card number'},
                    {view: 'text', label: 'Address', attributes: {maxlength: 40}, name: 'address'},
                    {view: 'text', localId: 'password', label: 'Password', type: 'password', name: 'password', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                    {view: 'datepicker', label: 'Date of birth', format: '%d.%m.%Y', name: 'dateOfBirth', invalidMessage: "Can't be empty"},
                    {view: 'text', localId: 'email', label: 'E-mail', attributes: {maxlength: 40}, name: 'email'},
                    {cols: [
                        {template: '<span class="fas fa-plus fa-2x"></span>',
                            borderless: true,
                            width: 50,
                            onClick: {
                                'fa-plus': () => {
                                    if (phoneAmount < 3) {
                                        phoneAmount++;
                                        this.$$(`phoneLine${phoneAmount}`).show();
                                    }
                                }
                            }
                        },
                        phonesLines
                    ]},
                    {
                        cols: [
                            {view: 'button',
                                value: 'Add',
                                localId: 'addButton',
                                click() {
                                    const values = this.$scope.$$('userForm').getValues();
                                    if (!values.readerCardNumber) {
                                        values.readerCardNumber = getRandomInt(100000, 200000);
                                    }
                                    emailCheck()
                                        .then(() => checkReaderCardNumber(values.readerCardNumber))
                                        .then(() => {
                                            if (this.$scope.$$('userForm').validate()) {
                                                const phonesArr = [];
                                                for (let i = 0; i < 4; i++) {
                                                    if (values[`phone${i}`] !== '') {
                                                        phonesArr.push(values[`phone${i}`]);
                                                    }
                                                }
                                                const phones = phonesArr.join(' ');
                                                values.phones = phones;
                                                if (this.getValue() === 'Add') {
                                                    this.$scope.$$('userForm').clear();
                                                    this.$scope.$$('userDetInfo').hide();
                                                    this.$scope.app.callEvent('addUser', [values]);
                                                }
                                                else {
                                                    this.$scope.$$('userForm').clear();
                                                    this.$scope.$$('userDetInfo').hide();
                                                    this.$scope.app.callEvent('editUser', [values]);
                                                }
                                            }
                                        });
                                }
                            },
                            {view: 'button',
                                value: 'Cancel',
                                click: () => {
                                    this.$$('userForm').clear();
                                    this.$$('userForm').clearValidation();
                                    phoneAmount = 0;
                                    webix.ui(phonesLines, this.$$('phonesLines'));
                                    this.$$('userDetInfo').hide();
                                }}
                        ]
                    }
                ],
                rules: {
                    firstName: webix.rules.isNotEmpty,
                    email: (value) => {
                        if (value.length === 0) {
                            this.$$('email').define('invalidMessage', "Can't be empty");
                            return false;
                        }
                        if (!webix.rules.isEmail(value)) {
                            this.$$('email').define('invalidMessage', 'Incorrect E-mail');
                            return false;
                        }
                        if (this.$$('email').check) {
                            this.$$('email').define('invalidMessage', 'This E-mail already have been taken');
                            return false;
                        }
                        return true;
                    },
                    phone0: webix.rules.isNotEmpty,
                    passportNumber: webix.rules.isNotEmpty,
                    dateOfBirth: value => value,
                    readerCardNumber: () => this.$$('readerCardNumber').check
                }
            }
        };
    }
    showPopup(item) {
        if (item) {
            const values = item;
            this.$$('addButton').setValue('Edit');
            const phonesArr = values.phones.split(' ');
            this.$$('userForm').setValues(values);
            for (let i = 0; i < phonesArr.length; i++) {
                this.$$(`phone${i}`).setValue(phonesArr[i]);
                this.$$(`phoneLine${i}`).show();
            }
        }
        else {
            this.$$('addButton').setValue('Add');
        }

        this.getRoot().show();
    }
}

export default UserDetInfo;
