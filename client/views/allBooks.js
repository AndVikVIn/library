import {JetView} from 'webix-jet';
import '../styles/app.css';
import BookDetails from './bookDetail';

class AllBooksPage extends JetView {
    config() {
        const createFormats = (book, format) => {
            if (book[format]) {
                const arr = [];
                const filesArr = book[format].split(';');
                for (let i = 0; i < filesArr.length; i++) {
                    const ext = filesArr[i].substr(filesArr[i].lastIndexOf('.'), filesArr[i].length);
                    const link = `<a href='${filesArr[i]}'>Download ${ext}</a>`;
                    arr.push(link);
                }
                return arr.join('<br>');
            }
            return '';
        };

        const makeSpecialFilter = (api) => {
            webix.ajax().get(api)
                .then((data) => {
                    const booksArr = data.json();
                    this.$$('booksTable').clearAll();
                    this.$$('booksTable').parse(booksArr);
                })
                .catch(() => {
                    webix.message('Server error. Please try again later.', 'error');
                });
        };

        const booksWithoutDigitalFormat = {
            view: 'button',
            height: 50,
            value: 'Books without digital formats',
            click: () => {
                makeSpecialFilter('api/getBooksWithoutDigitalFormat');
            }
        };

        const authorsWithMostBooks = {
            view: 'button',
            value: '10 authors who have more books',
            click: () => {
                makeSpecialFilter('api/authorsWithMostBooks');
            }
        };

        const specialSearch = {
            view: 'button',
            value: 'Books from 1980-2000 published in Spain',
            click: () => {
                makeSpecialFilter('api/specialSearch');
            }
        };

        const theOldest = {
            view: 'button',
            value: '10 oldest books',
            click: () => {
                makeSpecialFilter('api/filterByTime?time=old');
            }
        };

        const theNewest = {
            view: 'button',
            value: '10 newest books',
            click: () => {
                makeSpecialFilter('api/filterByTime?time=new');
            }
        };

        const longRead = {
            view: 'button',
            value: '10 books with biggest amount of pages',
            click: () => {
                makeSpecialFilter('api/longRead');
            }
        };

        const longName = {
            view: 'button',
            value: '10 books with longest name',
            click: () => {
                makeSpecialFilter('api/longName');
            }
        };


        const booksTable = {
            view: 'datatable',
            localId: 'booksTable',
            url: '/api/getAllBooks',
            save: '/api/changeBooks',
            select: true,
            datafetch: 25,
            loadahead: 25,
            fixedRowHeight: false,
            rowLineHeight: 25,
            rowHeight: 100,
            scheme: {
                $change: (obj) => {
                    const item = obj;
                    const format = webix.Date.strToDate('%Y.%m.%d');
                    item.year = format(obj.year);
                    item.author = `${item.authorFirstName} ${item.authorMiddleName} ${item.authorSurName}`;
                    return item;
                }
            },
            columns: [
                {id: 'post',
                    header: 'Poster',
                    width: 110,
                    template(obj) {
                        if (obj.poster === null || obj.poster === undefined || obj.poster === '') {
                            return "<img style='width:100px;height:110px' src='./img/bookDefault.png'>";
                        }
                        return `<img style='width:100px;height:110px' src='${obj.poster}'>`;
                    }},
                {id: 'title', header: ['Title', {content: 'serverFilter'}], sort: 'server', fillspace: true},
                {id: 'pages', header: ['Pages', {content: 'serverFilter'}], sort: 'server', fillspace: true},
                {id: 'year', header: ['Year', {content: 'serverFilter'}], format: webix.Date.dateToStr('%Y.%m.%d'), sort: 'server', fillspace: true},
                {id: 'author', header: ['Author', {content: 'serverFilter'}], fillspace: true},
                {id: 'publisher', header: ['Publisher', {content: 'serverFilter'}], sort: 'server', fillspace: true},
                {id: 'country', header: ['Country', {content: 'serverFilter'}], sort: 'server', fillspace: true},
                {id: 'genre', header: ['Genre', {content: 'serverFilter'}], sort: 'server', fillspace: true},
                {id: 'availableAmount', header: ['Available', {content: 'serverFilter'}], sort: 'server', fillspace: true},
                {id: 'rating', header: ['Rating', {content: 'serverFilter'}], sort: 'server', fillspace: true},
                {id: 'textFormats',
                    header: 'Text formats',
                    template: obj => createFormats(obj, 'textFormats'),
                    fillspace: true
                },
                {id: 'audioFormats',
                    header: 'Audio formats',
                    template: obj => createFormats(obj, 'audioFormats'),
                    fillspace: true
                }
            ],
            on: {
                onItemDblClick: () => {
                    const item = this.$$('booksTable').getSelectedItem();
                    if (item) {
                        this.bookDetails.showPopup(item);
                    }
                }
            }
        };
        return {
            rows: [
                {
                    cols: [
                        booksWithoutDigitalFormat,
                        authorsWithMostBooks,
                        specialSearch,
                        theOldest,
                        theNewest,
                        longRead,
                        longName
                    ]
                },
                booksTable
            ]
        };
    }
    init() {
        const user = this.app.getService('user').getUser();
        this.bookDetails = this.ui(BookDetails);
        this.on(this.app, 'increaseRating', (id, values) => {
            this.$$('booksTable').updateItem(id, values);
        });
        this.on(this.app, 'orderBook', (book) => {
            webix.ajax().get('/api/getOrderedBooks', {id: user.id})
                .then((res) => {
                    const alreadyOrderedBooks = res.text();
                    const books = alreadyOrderedBooks.split(';');
                    const isAlreadyHaveBook = books.find(title => title === book.title);
                    if (isAlreadyHaveBook) {
                        webix.message('You already ordered this book', 'info');
                        return;
                    }
                    const bookInTable = this.$$('booksTable').getItem(book.id);
                    if (bookInTable.availableAmount >= 1) {
                        bookInTable.availableAmount--;
                        this.$$('booksTable').updateItem(bookInTable.id, bookInTable);
                        books.push(book.title);
                        const orderedBooks = books.join(';');
                        webix.ajax().post('/api/setOrderedBooks', {id: user.id, orderedBooks})
                            .then(() => {
                                webix.message('Your request succesfully sent to librarian');
                            });
                    }
                })
                .catch(() => {
                    webix.message('Server error. Please try again later.', 'error');
                });
        });
    }
}

export default AllBooksPage;
