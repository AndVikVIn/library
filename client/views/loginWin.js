import {JetView} from 'webix-jet';

class LoginWin extends JetView {
    config() {
        return {
            view: 'window',
            type: 'clean',
            head: 'Login',
            position: 'center',
            height: 350,
            width: 600,
            body: {
                view: 'form',
                localId: 'logForm',
                elementsConfig: {
                    labelWidth: 150,
                    labelAlign: 'left'
                },
                elements: [
                    {view: 'text', label: 'E-mail Address', name: 'email', tooltip: "This field can't be empty"},
                    {view: 'text', label: 'Password', type: 'password', name: 'password', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                    {cols: [
                        {view: 'button', value: 'Login', inputWidth: 80, click: () => { this.doLogin(); }, hotkey: 'enter'},
                        {template: "<span class='passRec'>Forgot your password?</span>",
                            css: 'passForgot',
                            onClick: {
                                passRec: () => {
                                    this.app.callEvent('passRec');
                                }
                            }}
                    ]}
                ]
            }
        };
    }
    showWin() {
        this.getRoot().show();
    }
    hideWin() {
        this.getRoot().hide();
    }
    doLogin() {
        const user = this.app.getService('user');
        const form = this.$$('logForm');
        if (form.validate()) {
            const data = form.getValues();
            user.login(data).then(() => {
                this.app.config.access = user.getUser().role;
                if (this.app.config.access === 'admin') {
                    this.show('/adminPage');
                }
                else if (this.app.config.access === 'librarian') {
                    this.show('/librarianPage');
                }
                else {
                    this.show('/readerPage');
                }
            }).catch((err) => {
                if (!err.status) {
                    webix.html.removeCss(form.$view, 'invalid_login');
                    form.elements.password.focus();
                    webix.delay(() => {
                        webix.html.addCss(form.$view, 'invalid_login');
                        webix.message('Incorrect e-mail or password', 'info');
                    });
                }
                else {
                    webix.message('Serever error occured. Please try again later', 'error');
                }
            });
        }
    }
}

export default LoginWin;
