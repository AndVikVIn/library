import {JetView} from 'webix-jet';
import '../styles/app.css';
import UserDetInfo from './userDetInfo';


class AdminPage extends JetView {
    config() {
        const user = this.app.getService('user').getUser();

        const toolbar = {
            view: 'toolbar',
            localId: 'toolbar',
            cols: [
                {view: 'label',
                    label: 'Library',
                    css: 'headerLabel',
                    click: () => {
                        this.show('/adminPage/clientInfo');
                    }},
                {view: 'label', label: `Hello ${user.firstName}`, css: 'headerLabel'},
                {view: 'button',
                    value: 'Logout',
                    width: 100,
                    click: () => {
                        this.app.getService('user').logout().then(() => { this.app.show('afterLogout'); });
                    }}
            ]
        };

        const adminPanel = {
            view: 'datatable',
            header: 'Admin panel',
            localId: 'adminPanel',
            url: '/api/getAllUsers',
            save: '->/api/changeUsersData',
            select: true,
            scheme: {
                $init(obj) {
                    const item = obj;
                    const format = webix.Date.strToDate('%Y.%m.%d');
                    item.dateOfBirth = format(obj.dateOfBirth);

                    return item;
                }
            },
            fixedRowHeight: false,
            rowLineHeight: 25,
            rowHeight: 100,
            columns: [
                {id: 'role', header: 'Role', sort: 'string'},
                {id: 'firstName', header: ['First name', {content: 'textFilter'}], sort: 'string', fillspace: true},
                {id: 'middleName', header: ['Middle name', {content: 'textFilter'}], sort: 'string', fillspace: true},
                {id: 'lastName', header: ['Last name', {content: 'textFilter'}], sort: 'string', fillspace: true},
                {id: 'passportNumber', header: ['Passport #', {content: 'textFilter'}], sort: 'string', fillspace: true},
                {id: 'dateOfBirth', header: ['Date of birth', {content: 'dateFilter'}], format: webix.Date.dateToStr('%d %M %Y'), sort: 'date', fillspace: true},
                {id: 'address', header: ['Address', {content: 'textFilter'}], sort: 'string', fillspace: true},
                {id: 'phones', header: 'Phones', fillspace: true},
                {id: 'email', header: 'Email', fillspace: true}
            ],
            on: {
                onItemDblClick: () => {
                    const item = this.$$('adminPanel').getSelectedItem();
                    this.userDetInfo.showPopup(item);
                },
                onLoadError: (err) => {
                    webix.message(err.response, 'error');
                }
            }
        };

        const addButton = {
            view: 'button',
            localId: 'addButton',
            value: 'Add user',
            click: () => {
                this.userDetInfo.showPopup();
            }
        };
        const editButton = {
            view: 'button',
            localId: 'editButton',
            value: 'Edit user',
            click: () => {
                const item = this.$$('adminPanel').getSelectedItem();
                if (item) {
                    this.userDetInfo.showPopup(item);
                }
            }
        };
        const delButton = {
            view: 'button',
            localId: 'delButton',
            value: 'Delete user',
            click: () => {
                const item = this.$$('adminPanel').getSelectedItem();
                if (item) {
                    this.$$('adminPanel').remove(item.id);
                }
            }
        };

        return {
            rows: [
                toolbar,
                adminPanel,
                {cols: [
                    addButton,
                    editButton,
                    delButton
                ]}
            ]
        };
    }
    init() {
        const user = this.app.getService('user').getUser();
        this.app.config.access = user.role;
        this.userDetInfo = this.ui(UserDetInfo);
        this.on(this.app, 'addUser', (values) => {
            this.$$('adminPanel').add(values)
                .catch(() => {
                    webix.message('Server error occured. Please try again later.', 'error');
                });
        });
        this.on(this.app, 'editUser', (values) => {
            this.$$('adminPanel').updateItem(values.id, values)
                .catch(() => {
                    webix.message('Server error occured. Please try again later.', 'error');
                });
        });
    }
}

export default AdminPage;
