import {JetView} from 'webix-jet';

class PersonalPage extends JetView {
    config() {
        const emailCheck = () => {
            const value = this.$$('email').getValue();
            return webix.ajax().get('/api/checkEmail', {email: value})
                .then((data) => {
                    if (data.text() === '[]') {
                        this.$$('email').check = false;
                    }
                    else {
                        const emailOwnerId = data.json();
                        const editUserId = this.$$('personalDataForm').getValues().id;
                        if (emailOwnerId === editUserId) {
                            this.$$('email').check = false;
                        }
                        else {
                            this.$$('email').check = true;
                        }
                    }
                })
                .catch(() => {
                    webix.message('Server error. Please try again later.', 'error');
                });
        };
        let phoneAmount = 0;
        const hideLine = (num) => {
            this.$$(`phoneLine${num}`).hide();
            this.$$(`phone${num}`).setValue('');
            phoneAmount--;
        };
        const phonesLines = {
            localId: 'phonesLines',
            rows: [
                {localId: 'phoneLine0',
                    cols: [
                        {view: 'text', label: 'Phone', localId: 'phone0', name: 'phone0', pattern: {mask: '###-## #######', allow: /[0-9]/g}, placeholder: '375-## #######'}
                    ]},
                {localId: 'phoneLine1',
                    hidden: true,
                    cols: [
                        {view: 'text', label: 'Phone', localId: 'phone1', name: 'phone1', pattern: {mask: '###-## #######', allow: /[0-9]/g}, placeholder: '375-## #######'},
                        {template: '<i class="fas fa-times fa-2x"></i>', borderless: true, width: 30, onClick: {'fa-times': () => { hideLine(1); }}}
                    ]},
                {localId: 'phoneLine2',
                    hidden: true,
                    cols: [
                        {view: 'text', label: 'Phone', localId: 'phone2', name: 'phone2', pattern: {mask: '###-## #######', allow: /[0-9]/g}, placeholder: '375-## #######'},
                        {template: '<i class="fas fa-times fa-2x"></i>', borderless: true, width: 30, onClick: {'fa-times': () => { hideLine(2); }}}
                    ]},
                {localId: 'phoneLine3',
                    hidden: true,
                    cols: [
                        {view: 'text', label: 'Phone', localId: 'phone3', name: 'phone3', pattern: {mask: '###-## #######', allow: /[0-9]/g}, placeholder: '375-## #######'},
                        {template: '<i class="fas fa-times fa-2x"></i>', borderless: true, width: 30, onClick: {'fa-times': () => { hideLine(3); }}}
                    ]}
            ]};
        return {
            view: 'form',
            localId: 'personalDataForm',
            elementsConfig: {
                labelWidth: 200,
                labelAlign: 'left'
            },
            elements: [
                {type: 'header', template: 'Personal Data'},
                {view: 'text', label: 'First name', attributes: {maxlength: 20}, name: 'firstName', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                {view: 'text', label: 'Middle name', attributes: {maxlength: 20}, name: 'middleName', invalidMessage: "Can't be empty"},
                {view: 'text', label: 'Last name', attributes: {maxlength: 20}, name: 'lastName', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                {view: 'text', label: 'Passport #', attributes: {maxlength: 20}, name: 'passportNumber'},
                {view: 'text', label: 'Reader card number', attributes: {maxlength: 20}, name: 'readerCardNumber', disabled: true},
                {view: 'text', label: 'Address', attributes: {maxlength: 40}, name: 'address'},
                {view: 'text', localId: 'password', label: 'Password', type: 'password', name: 'password', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                {view: 'datepicker', label: 'Date of birth', format: '%d.%m.%Y', name: 'dateOfBirth', invalidMessage: "Can't be empty"},
                {view: 'text', localId: 'email', label: 'E-mail', attributes: {maxlength: 40}, name: 'email'},
                {cols: [
                    {template: '<span class="fas fa-plus fa-2x"></span>',
                        borderless: true,
                        width: 50,
                        onClick: {
                            'fa-plus': () => {
                                if (phoneAmount < 3) {
                                    phoneAmount++;
                                    this.$$(`phoneLine${phoneAmount}`).show();
                                }
                            }
                        }
                    },
                    phonesLines
                ]},
                {
                    cols: [
                        {view: 'button',
                            value: 'Edit',
                            localId: 'editButton',
                            click() {
                                emailCheck().then(() => {
                                    let values;
                                    if (this.$scope.$$('personalDataForm').validate()) {
                                        values = this.$scope.$$('personalDataForm').getValues();
                                        const phonesArr = [];
                                        for (let i = 0; i < 4; i++) {
                                            if (values[`phone${i}`] !== '') {
                                                phonesArr.push(values[`phone${i}`]);
                                            }
                                        }
                                        const phones = phonesArr.join(' ');
                                        values.phones = phones;
                                        webix.ajax().post('api/changeUsersData', values)
                                            .then(() => {
                                                webix.message('Your data succesfully changed!', 'info');
                                            })
                                            .catch(() => {
                                                webix.message('Something went wrong.Please try again.', 'error');
                                            });
                                    }
                                });
                            }
                        },
                        {view: 'button',
                            value: 'Cancel',
                            click: () => {
                                this.$$('personalDataForm').clear();
                                phoneAmount = 0;
                                webix.ui(phonesLines, this.$$('phonesLines'));
                            }}
                    ]
                }
            ],
            rules: {
                firstName: webix.rules.isNotEmpty,
                email: (value) => {
                    if (value.length === 0) {
                        this.$$('email').define('invalidMessage', "Can't be empty");
                        return false;
                    }
                    if (!webix.rules.isEmail(value)) {
                        this.$$('email').define('invalidMessage', 'Incorrect E-mail');
                        return false;
                    }
                    if (this.$$('email').check) {
                        this.$$('email').define('invalidMessage', 'This E-mail already have been taken');
                        return false;
                    }
                    return true;
                },
                phone0: webix.rules.isNotEmpty,
                passportNumber: webix.rules.isNotEmpty,
                dateOfBirth: value => value
            }
        };
    }
    init() {
        const user = this.app.getService('user').getUser();
        webix.ajax().get(`api/getOneUser?id=${user.id}`)
            .then((res) => {
                const values = res.json();
                const phonesArr = values.phones.split(' ');
                this.$$('personalDataForm').setValues(values);
                for (let i = 0; i < phonesArr.length; i++) {
                    this.$$(`phone${i}`).setValue(phonesArr[i]);
                    this.$$(`phoneLine${i}`).show();
                }
            })
            .catch(() => {
                webix.message('Server error. Please try again later.', 'error');
            });
    }
}

export default PersonalPage;

