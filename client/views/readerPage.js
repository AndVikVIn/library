import {JetView} from 'webix-jet';


class ReaderPage extends JetView {
    config() {
        const user = this.app.getService('user').getUser();
        const toolbar = {
            view: 'toolbar',
            localId: 'toolbar',
            cols: [
                {view: 'label', label: 'Library', css: 'headerLabel'},
                {view: 'label', label: `Hello ${user.firstName}`, css: 'headerLabel'},
                {view: 'button',
                    value: 'Logout',
                    width: 100,
                    click: () => {
                        this.app.getService('user').logout().then(() => { this.app.show('afterLogout'); });
                    }}
            ]
        };

        const readerNavList = {
            view: 'list',
            localId: 'navList',
            width: 250,
            select: true,
            scroll: false,
            template: '#value#',
            data: [
                {id: 'personalPage', value: 'Personal data'},
                {id: 'readerBookList', value: 'Order books'},
                {id: 'allBooks', value: 'All books'}
            ],
            on: {
                onAfterSelect: (obj) => {
                    this.app.show(`/readerPage/${obj}`);
                }
            }
        };

        return {
            rows: [
                toolbar,
                {
                    cols: [
                        readerNavList,
                        {$subview: true}
                    ]
                }
            ]
        };
    }
    init() {
        const user = this.app.getService('user').getUser();
        this.app.config.access = user.role;
    }
    ready(view, url) {
        if (url[1]) {
            this.$$('navList').select(url[1].page);
        }
        else {
            this.$$('navList').select(this.$$('navList').getFirstId());
        }
    }
}

export default ReaderPage;
