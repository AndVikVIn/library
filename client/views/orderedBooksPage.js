import {JetView} from 'webix-jet';
import ReaderDetInfo from './readerDetInfo';

class ReadersPage extends JetView {
    config() {
        const readersDatatable = {
            view: 'datatable',
            header: 'Readers',
            localId: 'readersTable',
            url: '/api/getReadersWithOrders',
            select: true,
            tooltip: true,
            scheme: {
                $init(obj) {
                    const item = obj;
                    const format = webix.Date.strToDate('%Y.%m.%d');
                    item.dateOfBirth = format(obj.dateOfBirth);
                    return item;
                }
            },
            fixedRowHeight: false,
            rowLineHeight: 25,
            rowHeight: 100,
            columns: [
                {id: 'firstName', header: ['First name', {content: 'textFilter'}], tooltip: 'Double click to edit readers books', sort: 'string', fillspace: true},
                {id: 'middleName', header: ['Middle name', {content: 'textFilter'}], tooltip: 'Double click to edit readers books', sort: 'string', fillspace: true},
                {id: 'lastName', header: ['Last name', {content: 'textFilter'}], tooltip: 'Double click to edit readers books', sort: 'string', fillspace: true},
                {id: 'passportNumber', header: ['Passport #', {content: 'textFilter'}], tooltip: 'Double click to edit readers books', sort: 'string', fillspace: true},
                {id: 'dateOfBirth', header: ['Date of birth', {content: 'dateFilter'}], format: webix.Date.dateToStr('%d %M %Y'), tooltip: 'Double click to edit readers books', sort: 'date', fillspace: true},
                {id: 'address', header: ['Address', {content: 'textFilter'}], tooltip: 'Double click to edit readers books', sort: 'string', fillspace: true},
                {id: 'phones', header: 'Phones', fillspace: true},
                {id: 'email', header: 'Email', fillspace: true}
            ],
            on: {
                onItemDblClick: () => {
                    const reader = this.$$('readersTable').getSelectedItem();
                    this.readerDetInfo.showWin(reader);
                }
            }
        };
        return readersDatatable;
    }
    init() {
        this.readerDetInfo = this.ui(ReaderDetInfo);
    }
}

export default ReadersPage;
