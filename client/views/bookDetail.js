import {JetView} from 'webix-jet';
import '../styles/app.css';


class BookDetails extends JetView {
    config() {
        const user = this.app.getService('user').getUser();

        const updateComment = (id, comment) => {
            webix.ajax().post('api/updateComment', {id, comment})
                .catch(() => {
                    webix.message('Server error occured. Please try again later.', 'error');
                });
        };
        const comments = {
            view: 'comments',
            localId: 'comments',
            currentUser: user.id,
            mentions: true,
            listItem: {
                templateText(obj) {
                    obj.text = obj.text.replace(/@/g, '<span class="mentions">@</span>');
                    let html = `<br><span class ='webix_comments_icons'></span><div class ='webix_comments_message'>${obj.text}</div>`;
                    html += '<span class="comment far fa-comment"></span>';
                    return html;
                }
            },
            on: {
                onAfterAdd: (id) => {
                    const comment = this.$$('comments').getItem(id);
                    const usersScope = this.$$('comments').getUsers();
                    const existingOfCurrentUser = usersScope.find(
                        userInScope => userInScope.id === user.id
                    );
                    if (!existingOfCurrentUser[0]) {
                        this.$$('comments').getUsers().add({id: user.id, value: user.firstName});
                    }
                    webix.ajax().post('api/saveComment', {comment})
                        .then((data) => {
                            const allComments = this.$$('comments').serialize();
                            const savedComment = data.json();
                            allComments[allComments.length - 1].id = savedComment.id;
                            const commentsIdArr = [];
                            this.$$('comments').clearAll();
                            this.$$('comments').parse(allComments);
                            allComments.forEach((commentItem) => {
                                commentsIdArr.push(commentItem.id);
                            });
                            webix.ajax().post('api/setUsersComments', {id: user.id, comment: savedComment.id});
                            webix.ajax().post('api/setBooksComments', {id: this.book.id, commentsIdArr});
                        })
                        .catch(() => {
                            webix.message('Server error occured. Please try again later.', 'error');
                        });
                },
                onDataUpdate: (id, obj) => {
                    updateComment(id, obj);
                },
                onAfterDelete: (id) => {
                    webix.ajax().del('api/removeOneComment', {id})
                        .catch(() => {
                            webix.message('Server error occured. Please try again later.', 'error');
                        });
                }
            }
        };

        return {
            view: 'popup',
            localId: 'addBookWin',
            position: 'center',
            modal: true,
            width: 1200,
            height: 800,
            body: {
                localId: 'bookDetails',
                rows: [
                    {
                        type: 'header',
                        localId: 'header',
                        align: 'center',
                        template: '#title# <span class="webix_icon wxi-close detInfoClose"></span>',
                        css: 'bookDetailHeader',
                        onClick: {
                            'wxi-close': () => {
                                this.$$('comments').clearAll();
                                this.$$('comments').getUsers().clearAll();
                                this.getRoot().hide();
                            }
                        }
                    },
                    {cols: [
                        {rows: [
                            {localId: 'poster',
                                template(obj) {
                                    if (obj.poster === null || obj.poster === undefined || obj.poster === '') {
                                        return "<img style='width:300px;height:220px' src='./img/bookDefault.png'>";
                                    }
                                    return `<img style='width:300px;height:220px' src='${obj.poster}'>`;
                                },
                                height: 220
                            },
                            {view: 'button', value: 'Print poster', width: 300, click: () => { webix.print(this.$$('poster')); }},
                            {view: 'button',
                                value: 'Order this book',
                                width: 300,
                                click: () => {
                                    this.app.callEvent('orderBook', [this.book]);
                                }
                            },
                            {}
                        ]},
                        {
                            localId: 'mainData',
                            template: '<div class="bookMainData"><span><b>Book title:</b> #title#</span><br><br>' +
                                '<span ><b>Pages:</b> #pages#</span><br><br><span><b>Year:</b> #year#</span><br><br>' +
                                '<span ><b>Author:</b> #authorFirstName# #authorMiddleName# #authorSurName#</span></div>' +
                                '<div class="bookMainData"><span ><b>Publisher:</b> #publisher#</span><br><br><span ><b>Country:</b> #country#</span><br><br>' +
                                '<span ><b>Genre:</b> #genre#</span><br><br><span ><b>Rating:</b> #rating# <span class="far fa-star"></span></span></div>',
                            onClick: {
                                'fa-star': () => {
                                    const item = this.$$('mainData').data;
                                    item.rating++;
                                    this.app.callEvent('increaseRating', [item.id, item]);
                                    this.$$('mainData').refresh();
                                    const rateStar = document.getElementsByClassName('fa-star');
                                    rateStar[0].classList.add('yellow');
                                    webix.ajax().post('/api/likeBook', {id: user.id, book: item.title});
                                }
                            }
                        }
                    ]},
                    comments
                ]
            }
        };
    }
    showPopup(item) {
        this.book = item;
        const user = this.app.getService('user').getUser();
        if (item) {
            const editedItem = item;
            editedItem.year = webix.Date.dateToStr('%Y')(item.year);
            this.$$('header').setValues(item);
            this.$$('poster').setValues(item);
            this.$$('mainData').setValues(editedItem);
            let commentsArr = [];
            let usersIds = [];
            webix.ajax().get('/api/getOneUser', {id: user.id})
                .then((data) => {
                    const currentUser = data.json();
                    const likedBooks = currentUser.likedBooks;
                    const likedBook = likedBooks.indexOf(item.title);
                    if (likedBook !== -1) {
                        const rateStar = document.getElementsByClassName('fa-star');
                        rateStar[0].classList.add('yellow');
                    }
                })
                .then(() => webix.ajax().get('/api/getBooksComments', {bookId: item.id}))
                .then((comments) => {
                    commentsArr = comments.json();
                    commentsArr.forEach((eachComment) => {
                        eachComment.date = new Date(eachComment.date);
                        eachComment.user_id = eachComment.userId;
                        usersIds.push(eachComment.userId);
                    });
                    return webix.ajax().get('api/getUsersCommentedBook', {ids: usersIds});
                })
                .then((users) => {
                    users = users.json();
                    this.$$('comments').parse(commentsArr);
                    users.forEach((commentedUser) => {
                        this.$$('comments').getUsers().add(commentedUser);
                    });
                    const commentsList = this.$$('comments').queryView({view: 'list'});
                    const text = this.$$('comments').queryView({view: 'textarea'});
                    commentsList.on_click.comment = (e, id) => {
                        text.setValue('');
                        const commentedComment = this.$$('comments').getItem(id);
                        const comUsers = this.$$('comments').getUsers();
                        text.setValue(`Reply on @${comUsers.getItem(commentedComment.user_id).firstName} message from ${commentedComment.date.toDateString()}:`);
                        text.focus();
                    };
                })
                .catch(() => {
                    webix.message('Server error occured. Please try again later.', 'error');
                });
        }
        this.getRoot().show();
    }
}

export default BookDetails;
