import {JetView} from 'webix-jet';
import '../styles/app.css';
import RegisterWin from './registerWin';
import LoginWin from './loginWin';
import PassRecWin from './passRecWin';

class LoginPage extends JetView {
    config() {
        const toolbar = {
            view: 'toolbar',
            localId: 'toolbar',
            cols: [
                {view: 'label', label: 'Library', css: 'headerLabel'},
                {view: 'button',
                    value: 'LogIn',
                    width: 100,
                    click: () => {
                        this.registerWin.hideWin();
                        this.loginWin.showWin();
                    }},
                {view: 'button',
                    value: 'Register',
                    width: 100,
                    click: () => {
                        this.loginWin.hideWin();
                        this.registerWin.showWin();
                    }}
            ]
        };
        const welcome = {
            template: 'Welcome to our Library!',
            css: 'welcomeMessage'
        };
        return {
            rows: [
                toolbar,
                welcome
            ]
        };
    }
    init() {
        this.registerWin = this.ui(RegisterWin);
        this.loginWin = this.ui(LoginWin);
        this.passRecWin = this.ui(PassRecWin);
        this.on(this.app, 'passRec', () => {
            this.loginWin.hideWin();
            this.passRecWin.showWin();
        });
    }
}

export default LoginPage;
