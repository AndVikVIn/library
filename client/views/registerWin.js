import {JetView} from 'webix-jet';

class RegisterWin extends JetView {
    config() {
        const emailCheck = () => {
            const value = this.$$('email').getValue();
            return webix.ajax().get('/api/checkEmail', {email: value}).then((data) => {
                if (data.text() === '[]') {
                    this.$$('email').check = false;
                }
                else {
                    this.$$('email').check = true;
                }
            }).catch(() => {
                webix.message('Server error. Please try again later.', 'error');
            });
        };

        const getRandomInt = (min, max) => Math.floor((Math.random() * (max - min)) + min);

        const checkReaderCardNumber = value => webix.ajax().get('api/checkCardNumber', {readerCardNumber: value})
            .then((data) => {
                if (data.text() !== '[]') {
                    const newValue = getRandomInt();
                    checkReaderCardNumber(newValue);
                }
                return value;
            });

        return {
            view: 'window',
            type: 'clean',
            head: 'Register',
            position: 'center',
            height: 650,
            width: 600,
            body: {
                view: 'form',
                localId: 'regForm',
                elementsConfig: {
                    labelWidth: 150,
                    labelAlign: 'left'
                },
                elements: [
                    {view: 'text', label: 'First name', name: 'firstName', attributes: {maxlength: 20}, tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                    {view: 'text', localId: 'email', label: 'E-mail Address', attributes: {maxlength: 20}, name: 'email', tooltip: "This field can't be empty"},
                    {view: 'text', localId: 'password', label: 'Password', type: 'password', name: 'password', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                    {view: 'text', localId: 'confirmPass', label: 'Confirm Password', type: 'password', name: 'confirmPass', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                    {view: 'text', label: 'Passport #', attributes: {maxlength: 20}, name: 'passportNumber', invalidMessage: "Can't be empty"},
                    {view: 'text', label: 'Phone', localId: 'phone0', name: 'phones', pattern: {mask: '###-## #######', allow: /[0-9]/g}, placeholder: '375-## #######', invalidMessage: "Can't be empty"},
                    {view: 'button',
                        value: 'Register',
                        inputWidth: 80,
                        click: () => {
                            const values = this.$$('regForm').getValues();
                            emailCheck()
                                .then(() => checkReaderCardNumber(getRandomInt(100000, 200000)))
                                .then((data) => {
                                    values.readerCardNumber = data;
                                    if (this.$$('regForm').validate()) {
                                        if (values.password === values.confirmPass) {
                                            webix.ajax().post('/api/newUser', values)
                                                .then(() => {
                                                    const user = this.app.getService('user');
                                                    return user.login(values);
                                                })
                                                .then(() => {
                                                    const user = this.app.getService('user');
                                                    this.app.config.access = user.getUser().role;
                                                    this.show('/readerPage');
                                                })
                                                .catch(() => {
                                                    webix.message('Server error occured. Please try again later.', 'error');
                                                });
                                        }
                                        else {
                                            webix.message('Incorrect password!', 'error');
                                        }
                                    }
                                }).catch(() => {
                                    webix.message('Server error occured. Please try again later.', 'error');
                                });
                        }}
                ],
                rules: {
                    firstName: webix.rules.isNotEmpty,
                    email: (value) => {
                        if (value.length === 0) {
                            this.$$('email').define('invalidMessage', "Can't be empty");
                            return false;
                        }
                        if (!webix.rules.isEmail(value)) {
                            this.$$('email').define('invalidMessage', 'Incorrect E-mail');
                            return false;
                        }
                        if (this.$$('email').check) {
                            this.$$('email').define('invalidMessage', 'This E-mail already have been taken');
                            return false;
                        }
                        return true;
                    },
                    password: webix.rules.isNotEmpty,
                    confirmPass: (value) => {
                        if (value.length === 0) {
                            this.$$('confirmPass').define('invalidMessage', "Can't be empty");
                            return false;
                        }
                        const confirmPass = this.$$('confirmPass').getValue();
                        const password = this.$$('password').getValue();
                        if (password !== confirmPass) {
                            this.$$('confirmPass').define('invalidMessage', 'Incorrect password confirmation');
                            return false;
                        }
                        return true;
                    },
                    phones: webix.rules.isNotEmpty,
                    passportNumber: webix.rules.isNotEmpty
                }
            }
        };
    }
    showWin() {
        this.getRoot().show();
    }
    hideWin() {
        this.getRoot().hide();
    }
}

export default RegisterWin;
