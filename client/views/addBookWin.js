import {JetView} from 'webix-jet';

class AddBookWin extends JetView {
    config() {
        const addPoster = {
            view: 'uploader',
            value: 'Add poster',
            inputName: 'poster',
            localId: 'posterUploader',
            link: 'mylist',
            inputWidth: 250,
            align: 'right',
            autosend: false,
            upload: '/api/upload'
        };
        const addTextFile = {
            view: 'uploader',
            value: 'Add text file',
            inputName: 'text',
            localId: 'textUploader',
            link: 'mylist1',
            inputWidth: 250,
            align: 'right',
            autosend: false,
            upload: '/api/upload'
        };
        const addAudioFile = {
            view: 'uploader',
            value: 'Add audio file',
            inputName: 'audio',
            localId: 'audioUploader',
            link: 'mylist2',
            inputWidth: 250,
            align: 'right',
            autosend: false,
            upload: '/api/upload'
        };


        const createFormatLines = (formatLine) => {
            const arr = [];
            if (typeof (formatLine) === 'object') {
                return '';
            }
            const filesFormatsArr = formatLine.split(';');
            const formatsAmount = filesFormatsArr.length;
            for (let i = 0; i < formatsAmount; i++) {
                const ext = filesFormatsArr[i].substr(filesFormatsArr[i].lastIndexOf('.'), filesFormatsArr[i].length);
                const line = `<span data-format=${i} class='fas fa-times formats'>  ${i + 1} ${ext}</span>`;
                arr.push(line);
            }
            return arr.join('<br>');
        };

        const deleteOneFormat = (formatLine, formats) => {
            const itemNumber = formatLine.target.dataset.format;
            formatLine.target.parentNode.removeChild(formatLine.target);
            const formatsArr = this.$$('bookForm').getValues()[formats].split(';');
            formatsArr.splice(itemNumber, 1);
            this.$$('bookForm').setValues({[formats]: formatsArr.join(';')}, true);
            return false;
        };

        const clearUploaders = () => {
            this.$$('posterUploader').files.data.clearAll();
            this.$$('textUploader').files.data.clearAll();
            this.$$('audioUploader').files.data.clearAll();
        };

        const clearFormatsLine = () => {
            const formats = document.getElementsByClassName('formats');
            const length = formats.length;
            for (let i = length - 1; i >= 0; --i) {
                formats[i].remove();
            }
            this.$$('bookForm').setValues({textFormats: '', audioFormats: ''}, true);
        };

        return {
            view: 'popup',
            localId: 'addBookWin',
            position: 'center',
            modal: true,
            width: 1200,
            body: {
                view: 'form',
                localId: 'bookForm',
                elementsConfig: {
                    labelWidth: 200,
                    labelAlign: 'center',
                    padding: 10
                },
                elements: [
                    {type: 'header', template: 'Add book to the Library'},
                    {cols: [
                        {rows: [
                            {view: 'text', label: 'Title', attributes: {maxlength: 50}, name: 'title', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                            {view: 'datepicker', label: 'Release date', format: '%d.%m.%Y', name: 'year', invalidMessage: "Can't be empty"},
                            {view: 'text', label: 'Pages', name: 'pages', tooltip: "This field can't be empty", invalidMessage: 'Please, use digits'},
                            {view: 'text', label: 'Author firstname', attributes: {maxlength: 20}, name: 'authorFirstName', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                            {view: 'text', label: 'Author middlename', attributes: {maxlength: 20}, name: 'authorMiddleName'},
                            {view: 'text', label: 'Author surname', attributes: {maxlength: 20}, name: 'authorSurName', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"}
                        ]},
                        {rows: [
                            {view: 'text', label: 'Publisher', attributes: {maxlength: 40}, name: 'publisher', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                            {view: 'text', label: 'Release country', attributes: {maxlength: 20}, name: 'country', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                            {view: 'text', label: 'Amount', attributes: {maxlength: 20}, name: 'availableAmount', tooltip: "This field can't be empty", invalidMessage: 'Please, use digits'},
                            {view: 'text', label: 'Rating', name: 'rating', tooltip: "This field can't be empty", invalidMessage: "Can't be empty"},
                            {view: 'text', label: 'Genre', attributes: {maxlength: 20}, name: 'genre', tooltip: "This field can't be empty", invalidMessage: 'Please, use digits'}
                        ]}
                    ]},
                    {cols: [
                        {rows: [
                            {},
                            {view: 'list', id: 'mylist', type: 'uploader', autoheight: true, borderless: true},
                            addPoster
                        ]},
                        {rows: [
                            {view: 'forminput',
                                label: 'Text Formats',
                                name: 'textFormats',
                                localId: 'textFormats',
                                body: {
                                    width: 125,
                                    height: 100,
                                    borderless: true,
                                    template: formatLine => createFormatLines(formatLine),
                                    onClick: {
                                        'fa-times': formatLine => deleteOneFormat(formatLine, 'textFormats')
                                    }
                                }
                            },
                            {view: 'list', id: 'mylist1', type: 'uploader', autoheight: true, borderless: true},
                            addTextFile
                        ]},
                        {rows: [
                            {view: 'list', id: 'mylist2', type: 'uploader', autoheight: true, borderless: true},
                            {view: 'forminput',
                                label: 'Audio Formats',
                                name: 'audioFormats',
                                localId: 'audioFormats',
                                body: {
                                    width: 125,
                                    height: 100,
                                    borderless: true,
                                    template: formatLine => createFormatLines(formatLine),
                                    onClick: {
                                        'fa-times': formatLine => deleteOneFormat(formatLine, 'audioFormats')
                                    }
                                }
                            },
                            addAudioFile
                        ]}
                    ]},
                    {
                        cols: [
                            {view: 'button',
                                value: 'Add',
                                localId: 'addButton',
                                click() {
                                    if (this.$scope.$$('bookForm').validate()) {
                                        const values = this.$scope.$$('bookForm').getValues();
                                        this.$scope.$$('posterUploader').send((poster) => {
                                            if (poster) {
                                                values.poster = poster.path;
                                            }
                                            this.$scope.$$('textUploader').send((text) => {
                                                if (typeof values.textFormats !== 'string') {
                                                    delete values.textFormats;
                                                }
                                                if (text) {
                                                    const textFiles = [];
                                                    this.$scope.$$('textUploader').files.data.each((file) => {
                                                        if (file.status === 'server') {
                                                            textFiles.push(file.path);
                                                        }
                                                    });
                                                    if (values.textFormats) {
                                                        values.textFormats = [values.textFormats, ...textFiles].join(';');
                                                    }
                                                    else { values.textFormats = textFiles.join(';'); }
                                                }
                                                this.$scope.$$('audioUploader').send((audio) => {
                                                    if (typeof values.audioFormats !== 'string') {
                                                        delete values.audioFormats;
                                                    }
                                                    if (audio) {
                                                        const audioFiles = [];
                                                        this.$scope.$$('audioUploader').files.data.each((file) => {
                                                            if (file.status === 'server') {
                                                                audioFiles.push(file.path);
                                                            }
                                                        });
                                                        if (values.audioFormats) {
                                                            values.audioFormats = [values.audioFormats, ...audioFiles].join(';');
                                                        }
                                                        else { values.audioFormats = audioFiles.join(';'); }
                                                    }
                                                    if (this.getValue() === 'Add') {
                                                        clearUploaders();
                                                        this.$scope.$$('bookForm').clear();
                                                        this.$scope.$$('addBookWin').hide();
                                                        this.$scope.app.callEvent('addBook', [values]);
                                                    }
                                                    else {
                                                        clearUploaders();
                                                        clearFormatsLine();
                                                        this.$scope.$$('bookForm').clear();
                                                        this.$scope.$$('addBookWin').hide();
                                                        this.$scope.app.callEvent('editBook', [values]);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                }
                            },
                            {view: 'button',
                                value: 'Cancel',
                                click: () => {
                                    clearUploaders();
                                    clearFormatsLine();
                                    this.$$('bookForm').clear();
                                    this.$$('addBookWin').hide();
                                }}
                        ]
                    }
                ],
                rules: {
                    title: webix.rules.isNotEmpty,
                    year: webix.rules.isNotEmpty,
                    pages: (value) => {
                        if (value) {
                            return !!webix.rules.isNumber(value);
                        }
                        return false;
                    },
                    authorFirstName: webix.rules.isNotEmpty,
                    authorSurName: webix.rules.isNotEmpty,
                    publisher: webix.rules.isNotEmpty,
                    country: webix.rules.isNotEmpty,
                    genre: webix.rules.isNotEmpty,
                    availableAmount: (value) => {
                        if (value) {
                            return !!webix.rules.isNumber(value);
                        }
                        return false;
                    },
                    rating: (value) => {
                        if (value) {
                            return !!webix.rules.isNumber(value);
                        }
                        return false;
                    }
                }
            }
        };
    }
    showPopup(item) {
        if (item) {
            this.$$('addButton').setValue('Edit');
            this.$$('bookForm').setValues(item);
        }
        else {
            this.$$('addButton').setValue('Add');
        }
        this.getRoot().show();
    }
}

export default AddBookWin;
