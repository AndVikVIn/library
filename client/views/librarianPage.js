import {JetView} from 'webix-jet';
import '../styles/app.css';

class LibrarianPage extends JetView {
    config() {
        const user = this.app.getService('user').getUser();
        const toolbar = {
            view: 'toolbar',
            localId: 'toolbar',
            cols: [
                {view: 'label', label: 'Library', css: 'headerLabel'},
                {view: 'label', label: `Hello ${user.firstName}`, css: 'headerLabel'},
                {view: 'button',
                    value: 'Logout',
                    width: 100,
                    click: () => {
                        this.app.getService('user').logout().then(() => { this.app.show('afterLogout'); });
                    }}
            ]
        };

        const librarianNavList = {
            view: 'list',
            localId: 'navList',
            width: 250,
            select: true,
            scroll: false,
            template: '#value#',
            data: [
                {id: 'readersPage', value: 'Readers'},
                {id: 'booksPage', value: 'Books'},
                {id: 'orderedBooksPage', value: 'Orders'}
            ],
            on: {
                onAfterSelect: (obj) => {
                    this.app.show(`/librarianPage/${obj}`);
                }
            }
        };

        return {
            rows: [
                toolbar,
                {
                    cols: [
                        librarianNavList,
                        {$subview: true}
                    ]
                }
            ]
        };
    }
    init() {
        const user = this.app.getService('user').getUser();
        this.app.config.access = user.role;
    }
    ready(view, url) {
        if (url[1]) {
            this.$$('navList').select(url[1].page);
        }
        else {
            this.$$('navList').select(this.$$('navList').getFirstId());
        }
    }
}

export default LibrarianPage;
