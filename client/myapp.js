/* eslint-disable no-undef */
import {JetApp, EmptyRouter, HashRouter, plugins} from 'webix-jet';
import './styles/app.css';
import session from './models/session';

export default class MyApp extends JetApp {
    constructor(config) {
        const defaults = {
            id: APPNAME,
            version: VERSION,
            router: BUILD_AS_MODULE ? EmptyRouter : HashRouter,
            debug: !PRODUCTION,
            start: '/loginPage'
        };

        super({...defaults, ...config});
        this.use(plugins.User, {
            model: session,
            login: '/loginPage',
            afterLogout: '/loginPage'
        });
    }
}
if (!BUILD_AS_MODULE) {
    webix.ready(() => {
        const app =	new MyApp();
        app.attachEvent('app:guard', (url, view, nav) => {
            const navigate = nav;
            if (app.config.access === 'reader') {
                if (url.indexOf('/adminPage') !== -1) {
                    webix.message('Access denied', 'error');
                    navigate.redirect = '/readerPage';
                }
                else if (url.indexOf('/librarianPage') !== -1) {
                    webix.message('Access denied', 'error');
                    navigate.redirect = '/readerPage';
                }
            }
            else if (app.config.access === 'librarian') {
                if (url.indexOf('/adminPage') !== -1) {
                    webix.message('Access denied', 'error');
                    navigate.redirect = '/librarianPage';
                }
                else if (url.indexOf('/readerPage') !== -1) {
                    webix.message('Access denied', 'error');
                    navigate.redirect = '/librarianPage';
                }
            }
        });
        app.render();
    });
}
