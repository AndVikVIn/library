module.exports = {
    up(queryInterface, Sequelize) {
        return queryInterface.addColumn(
            'Comments',
            'userId',
            {
                type: Sequelize.INTEGER,
                references: {
                    model: 'users',
                    key: 'id'
                }

            }
        );
    }
};
