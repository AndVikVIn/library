const multer = require('multer');

const locations = [
    {
        location: './public/posters',
        ext: ['.jpeg', '.png', '.jfif', '.jpg']
    },
    {
        location: './public/textFormats',
        ext: ['.pdf', '.txt', '.rtf', '.doc', '.docx', '.fb2', '.fb2', '.epub', '.djvu']
    },
    {
        location: './public/audioFormats',
        ext: ['.flac', '.mp3', '.mp4']
    }
];

const findLocation = (extension, callback) => {
    const length = locations.length;
    for (let i = 0; i < length; i++) {
        const extLength = locations[i].ext.length;
        for (let q = 0; q < extLength; q++) {
            if (locations[i].ext[q] === extension) {
                return callback(null, locations[i].location);
            }
        }
    }
    return false;
};

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
        findLocation(ext, cb);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});


const upload = multer({storage});

module.exports = upload;
