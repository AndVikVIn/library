const wrap = require('./wrap');
const express = require('express');
const sequelize = require('../dataBase');
const upload = require('../middleware/fileUploader');
const changeData = require('./changeData');

const router = express.Router();
const book = sequelize.import('../models/book');

const changeBooksData = wrap(changeData(book));
const returnPath = (req, res) => {
    const file = req.files.poster || req.files.text || req.files.audio;
    const path = file[0].path.substring(6);
    res.send({path});
};

const getAll = wrap(async (req, res) => {
    const result = await (book.getAll(req.query.sort, req.query.start, req.query.count, req.query.filter));
    res.send({data: result.books, pos: req.query.start, total_count: result.totalAmount});
});

const getOne = wrap(async (req, res) => {
    res.send(await book.getOne(req.body.id));
});

const updateMany = wrap(async (req, res) => {
    res.send(await book.updateMany(req.body.books));
});

const getBooksComments = wrap(async (req, res) => {
    res.send(await book.getBooksComments(req.query.bookId));
});
const setBooksComments = wrap(async (req, res) => {
    res.send(await book.setBooksComments(req.body.id, req.body.commentsIdArr));
});
const getBooksWithoutDigitalFormat = wrap(async (req, res) => {
    res.send(await book.getBooksWithoutDigitalFormat());
});
const authorsWithMostBooks = wrap(async (req, res) => {
    res.send(await book.authorsWithMostBooks());
});
const specialSearch = wrap(async (req, res) => {
    res.send(await book.specialSearch());
});

const filterByTime = wrap(async (req, res) => {
    res.send(await book.filterByTime(req.query.time));
});

const longRead = wrap(async (req, res) => {
    res.send(await book.longRead());
});

const longName = wrap(async (req, res) => {
    res.send(await book.longName());
});

router.get('/getAllBooks', getAll);
router.get('/getOneBook', getOne);
router.post('/changeBooks', changeBooksData);
router.post('/updateManyBooks', updateMany);
router.get('/getBooksComments', getBooksComments);
router.post('/setBooksComments', setBooksComments);
router.post('/upload', upload.fields([{name: 'poster'}, {name: 'text'}, {name: 'audio'}]), returnPath);
router.get('/getBooksWithoutDigitalFormat', getBooksWithoutDigitalFormat);
router.get('/authorsWithMostBooks', authorsWithMostBooks);
router.get('/specialSearch', specialSearch);
router.get('/filterByTime', filterByTime);
router.get('/longRead', longRead);
router.get('/longName', longName);

module.exports = router;
