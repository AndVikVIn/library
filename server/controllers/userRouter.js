const wrap = require('./wrap');
const express = require('express');
const sequelize = require('../dataBase');
const passport = require('passport');
const adminCheck = require('../middleware/adminCheck');
const changeData = require('./changeData');
require('../passportStrategy');


const router = express.Router();
const user = sequelize.import('../models/user');

const changeUserData = wrap(changeData(user));

const createNew = wrap(async (req, res) => {
    res.send(await user.createNew(req.body));
});

const getAll = wrap(async (req, res) => {
    res.send(await user.getAll());
});

const getOne = wrap(async (req, res) => {
    const authorizedUser = req.session.user;
    const userId = Number(req.query.id);
    if (userId === authorizedUser.id) {
        res.send(await user.getOne(userId));
    }
    else {
        res.status(403).send('Access denied');
    }
});

const checkEmail = wrap(async (req, res) => {
    res.send(await user.checkEmail(req.query.email));
});

const checkCardNumber = wrap(async (req, res) => {
    res.send(await user.checkCardNumber(req.query.readerCardNumber));
});

const logout = wrap(async (req, res) => {
    req.session.destroy(() => {
        res.send({});
    });
});

const sendStatus = wrap(async (req, res) => {
    res.send(req.session.user || null);
});

const login = wrap(async (req, res, next) => {
    passport.authenticate('local', (err, logUser) => {
        if (err) return next(err);
        if (!logUser) res.send({});
        req.logIn(logUser, (error) => {
            if (error) return next(err);
            req.session.user = logUser;
            res.send(logUser);
            return false;
        });
        return false;
    })(req, res, next);
});

const getUsersBooks = wrap(async (req, res) => {
    res.send(await user.getUsersBooks(req.query.id));
});

const setUsersBooks = wrap(async (req, res) => {
    const role = req.session.user.role;
    if (role !== 'librarian') {
        res.status(403).send('Access denied');
    }
    res.send(await user.setUsersBooks(req.body.id, req.body));
});

const getOrderedBooks = wrap(async (req, res) => {
    res.send(await user.getOrderedBooks(req.query.id));
});

const setOrderedBooks = wrap(async (req, res) => {
    res.send(await user.setOrderedBooks(req.body.id, req.body));
});

const getReaders = wrap(async (req, res) => {
    res.send(await user.getReaders());
});

const getReadersWithOrders = wrap(async (req, res) => {
    res.send(await user.getReadersWithOrders());
});

const likeBook = wrap(async (req, res) => {
    res.send(await user.likeBook(req.body.id, req.body.book));
});

const setUsersComments = wrap(async (req, res) => {
    res.send(await user.setUsersComments(req.body.id, req.body.comment));
});

const getUsersComments = wrap(async (req, res) => {
    res.send(await user.getUsersComments(req.body.id));
});

const getUsersCommentedBook = wrap(async (req, res) => {
    res.send(await user.getUsersCommentedBook(req.query.ids));
});

router.get('/getReaders', getReaders);
router.get('/getReaderBooks', getUsersBooks);
router.post('/setReaderBooks', setUsersBooks);
router.post('/login', login);
router.post('/logout', logout);
router.post('/login/status', sendStatus);
router.post('/newUser', createNew);
router.post('/changeUsersData', changeUserData);
router.get('/getAllUsers', adminCheck, getAll);
router.get('/getOneUser', getOne);
router.get('/checkEmail', checkEmail);
router.get('/checkCardNumber', checkCardNumber);
router.get('/getOrderedBooks', getOrderedBooks);
router.post('/setOrderedBooks', setOrderedBooks);
router.get('/getReadersWithOrders', getReadersWithOrders);
router.post('/likeBook', likeBook);
router.post('/setUsersComments', setUsersComments);
router.get('/getUsersComments', getUsersComments);
router.get('/getUsersCommentedBook', getUsersCommentedBook);

module.exports = router;
