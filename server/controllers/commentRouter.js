const wrap = require('./wrap');
const express = require('express');
const sequelize = require('../dataBase');

const comment = sequelize.import('../models/comments');
const router = express.Router();

const createNew = wrap(async (req, res) => {
    res.send(await comment.createNew(req.body.comment));
});

const updateOne = wrap(async (req, res) => {
    res.send(await comment.updateOne(req.body.id, req.body.comment));
});

const removeOne = wrap(async (req, res) => {
    const result = await comment.removeOne(req.body.id);
    if (result === 1) {
        res.json({
            code: 200,
            message: 'Ok',
            response: {msg: 'Recored deleted'}
        });
    }
    else {
        res.json({
            code: 404,
            message: 'Ok',
            response: {msg: 'Not found'}
        });
    }
});

router.post('/saveComment', createNew);
router.post('/updateComment', updateOne);
router.delete('/removeOneComment', removeOne);

module.exports = router;
