const express = require('express');

const router = express.Router();

router.use(require('./userRouter'));
router.use(require('./bookRouter'));
router.use(require('./commentRouter'));

module.exports = router;
