const sequelize = require('./dataBase');
const express = require('express');
const bodyParser = require('body-parser');
const router = require('./controllers/mainRouter');
const session = require('express-session');
const SequelizeStore = require('connect-session-sequelize')(session.Store);
const passport = require('passport');

const app = express();

function errorHandler(err, req, res) {
    res.status(500);
    res.render('error', {error: err});
}

app.use(session({
    secret: 'asdadqweqweqweqeqasdasd',
    store: new SequelizeStore({
        db: sequelize
    }),
    resave: false,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}));


app.listen(3000);

app.use('/api', router);

app.use(errorHandler);

process.on('SIGINT', () => {
    sequelize.close();
    process.exit();
});

