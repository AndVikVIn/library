const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const sequelize = require('./dataBase');

const User = sequelize.import('./models/user');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findByPk(id).then((user, err) => {
        if (err) {
            done(err);
        }
        else { done(null, user); }
    });
});

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, ((username, password, done) => {
        User.findOne({where: {email: username}}).then(async (user, err) => {
            if (err) return done(err);
            if (!user) return done(null, false, {message: 'Incorrect username.'});
            const match = await bcrypt.compare(password, user.password);
            if (!match) return done(null, false, {message: 'Incorrect password.'});
            return done(null, user);
        });
    })
));

