module.exports = (sequelize, DataTypes) => {
    const comment = sequelize.define('Comment', {
        date: DataTypes.DATE,
        text: DataTypes.STRING
    }, {});

    comment.createNew = async (newComment) => {
        const parsedComment = JSON.parse(newComment);
        delete parsedComment.id;
        const createdComment = await comment.create(parsedComment);
        return createdComment;
    };

    comment.updateOne = async (id, values) => {
        values = JSON.parse(values);
        const result = await comment.update(values,
            {where: {id}}
        );
        return result;
    };

    comment.removeOne = async (id) => {
        const result = await comment.destroy({where: {id}});
        return result;
    };

    return comment;
};
