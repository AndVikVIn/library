const Sequelize = require('sequelize');

const Op = Sequelize.Op;

module.exports = (sequelize, DataTypes) => {
    const book = sequelize.define('book', {
        title: {
            type: DataTypes.STRING
        },
        pages: {
            type: DataTypes.INTEGER
        },
        year: {
            type: DataTypes.DATEONLY
        },
        authorFirstName: {
            type: DataTypes.STRING
        },
        authorMiddleName: {
            type: DataTypes.STRING
        },
        authorSurName: {
            type: DataTypes.STRING
        },
        publisher: {
            type: DataTypes.STRING
        },
        country: {
            type: DataTypes.STRING
        },
        genre: {
            type: DataTypes.STRING
        },
        availableAmount: {
            type: DataTypes.INTEGER
        },
        rating: {
            type: DataTypes.INTEGER
        },
        textFormats: {
            type: DataTypes.STRING
        },
        audioFormats: {
            type: DataTypes.STRING
        },
        poster: {
            type: DataTypes.STRING
        }
    });
    book.createNew = async (values) => {
        const result = await book.create(values);
        return result;
    };
    book.updateOne = async (id, values) => {
        const item = values;
        if (item.userId === '') {
            delete item.userId;
        }
        const result = await book.update(item, {where: {id}});
        return result;
    };
    book.updateMany = (books) => {
        const result = [];
        const editedBooks = JSON.parse(books);
        editedBooks.forEach(async (editedBook) => {
            result.push(await book.updateOne(editedBook.id, editedBook));
        });
        return result;
    };
    book.deleteOne = async (id) => {
        const result = await book.destroy({where: {id}});
        return result;
    };
    book.getAll = async (sort, start, count, filter) => {
        const result = {
            books: [],
            totalAmount: 0
        };
        let allLibraryBooks = [];
        const filterConditions = {};
        if (filter) {
            const filterPropertiesArr = Object.entries(filter);
            filterPropertiesArr.forEach((item) => {
                if (item[1] !== '') {
                    if (item[0] === 'author') {
                        filterConditions[Op.or] = {
                            authorFirstName: {
                                [Op.substring]: item[1]
                            },
                            authorMiddleName: {
                                [Op.substring]: item[1]
                            },
                            authorSurName: {
                                [Op.substring]: item[1]
                            }
                        };
                    }
                    else {
                        filterConditions[item[0]] = {[Op.substring]: item[1]};
                    }
                }
            });
        }
        if (sort) {
            const sortOrder = Object.entries(sort);
            allLibraryBooks = await book.findAll({
                order: sortOrder
            });
        }
        else {
            allLibraryBooks = await book.findAll({
                where: filterConditions
            });
        }
        let amount = Number(start) + Number(count);
        for (let i = start; i <= amount; i++) {
            if (allLibraryBooks[i]) {
                result.books.push(allLibraryBooks[i]);
            }
        }
        result.totalAmount = allLibraryBooks.length - 1;
        return result;
    };
    book.getOne = async (id) => {
        const result = await book.findOne({where: {id}});
        return result;
    };
    book.getBooksWithoutDigitalFormat = async () => {
        const result = await book.findAll({
            where: {
                textFormats: ''
            }
        });
        return result;
    };

    const sortConditions = (value1, value2) => {
        let result = 0;
        if (value1 > value2) {
            result = -1;
        }
        else if (value1 < value2) {
            result = 1;
        }
        return result;
    };

    book.authorsWithMostBooks = async () => {
        const allLibraryBooks = await book.findAll({});
        const authors = [];
        allLibraryBooks.forEach((eachBook) => {
            const authorFullName = `${eachBook.dataValues.authorFirstName} ${eachBook.dataValues.authorMiddleName} ${eachBook.dataValues.authorLastName}`;
            eachBook.dataValues.author = authorFullName;
            const existAuthor = authors.find(
                currentBook => eachBook.dataValues.author === currentBook.author
            );
            if (!existAuthor) {
                const newAuthor = {
                    author: eachBook.dataValues.author,
                    books: [eachBook.dataValues]
                };
                authors.push(newAuthor);
            }
            else {
                existAuthor.books.push(eachBook.dataValues);
            }
        });
        authors.sort(
            (author1, author2) => sortConditions(author1.books.length, author2.books.length)
        );
        const authorsWithMostBooksArr = authors.splice(0, 10);
        let booksArr = [];
        authorsWithMostBooksArr.forEach((currentAuthor) => {
            booksArr = booksArr.concat(currentAuthor.books);
        });
        return booksArr;
    };

    book.specialSearch = async () => {
        const booksFromSpain = await book.findAll({
            where: {
                country: 'Spain',
                year: {
                    [Op.between]: [new Date('1980-01-01'), new Date('2000-01-01')]
                }
            }
        });
        return booksFromSpain;
    };

    book.filterByTime = async (time) => {
        const allLibraryBooks = await book.findAll({});
        allLibraryBooks.sort((book1, book2) => {
            let result = 0;
            if (time === 'old') {
                if (book1.dataValues.year.toString() > book2.dataValues.year.toString()) {
                    result = 1;
                }
                else if (book1.dataValues.year.toString() < book2.dataValues.year.toString()) {
                    result = -1;
                }
            }
            if (time === 'new') {
                if (book1.dataValues.year.toString() > book2.dataValues.year.toString()) {
                    result = -1;
                }
                else if (book1.dataValues.year.toString() < book2.dataValues.year.toString()) {
                    result = 1;
                }
            }
            return result;
        });
        const booksArr = allLibraryBooks.splice(0, 10);
        return booksArr;
    };

    book.longRead = async () => {
        const allLibraryBooks = await book.findAll({});
        allLibraryBooks.sort(
            (book1, book2) => sortConditions(book1.dataValues.pages, book2.dataValues.pages)
        );
        const booksArr = allLibraryBooks.splice(0, 10);
        return booksArr;
    };

    book.longName = async () => {
        const allLibraryBooks = await book.findAll({});
        allLibraryBooks.sort(
            (book1, book2) => sortConditions(
                book1.dataValues.title.length, book2.dataValues.title.length
            )
        );
        const booksArr = allLibraryBooks.splice(0, 10);
        return booksArr;
    };

    const Comment = sequelize.import('../models/comments');

    book.hasMany(Comment, {as: 'Comments'});
    Comment.belongsTo(book);

    book.setBooksComments = async (id, commentsId) => {
        const currentBook = await book.findOne({where: {id}});
        const comments = JSON.parse(commentsId);
        const result = await currentBook.setComments(comments);
        return result;
    };

    book.getBooksComments = async (id) => {
        const currentBook = await book.findOne({where: {id}});
        const result = await currentBook.getComments();
        return result;
    };


    return book;
};
