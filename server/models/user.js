const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');

const Op = Sequelize.Op;
const saltRounds = 10;

module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('user', {
        firstName: {
            type: DataTypes.STRING
        },
        middleName: {
            type: DataTypes.STRING
        },
        lastName: {
            type: DataTypes.STRING
        },
        passportNumber: {
            type: DataTypes.STRING
        },
        dateOfBirth: {
            type: DataTypes.DATEONLY
        },
        address: {
            type: DataTypes.STRING
        },
        phones: {
            type: DataTypes.STRING
        },
        readerCardNumber: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING
        },
        orderedBooks: {
            type: DataTypes.STRING
        },
        likedBooks: {
            type: DataTypes.STRING
        },
        role: {
            type: DataTypes.ENUM,
            values: ['admin', 'reader', 'librarian'],
            defaultValue: 'reader'
        }
    });
    user.createNew = async (values) => {
        const newUser = values;
        const hashPass = await bcrypt.hash(values.password, saltRounds);
        newUser.password = hashPass;
        newUser.role = 'reader';
        const result = await user.create(newUser);
        return result;
    };
    user.updateOne = async (id, values) => {
        const editableUser = await user.findOne({where: {id}});
        const userValues = values;
        if (editableUser.dataValues.password !== userValues.password) {
            const hashPass = await bcrypt.hash(values.password, saltRounds);
            userValues.password = hashPass;
        }
        const result = await user.update(userValues,
            {where: {id}}
        );
        return result;
    };
    user.deleteOne = async (id) => {
        const result = await user.destroy({where: {id}});
        return result;
    };
    user.checkEmail = async (obj) => {
        const result = await user.findOne({
            where: {email: obj}
        });
        if (result) {
            const userId = JSON.stringify(result.dataValues.id);
            return userId;
        }
        return result;
    };
    user.checkCardNumber = async (obj) => {
        const result = await user.findOne({
            where: {readerCardNumber: obj}
        });
        if (result) {
            const userId = JSON.stringify(result.dataValues.id);
            return userId;
        }
        return result;
    };
    user.getAll = async () => {
        const result = await user.findAll();
        return result;
    };
    user.getOne = async (id) => {
        const result = await user.findOne({
            where: {id}
        });
        return result;
    };
    const Book = sequelize.import('../models/book');
    user.hasMany(Book, {as: 'Books'});
    Book.belongsTo(user);

    const Comment = sequelize.import('../models/comments');
    user.hasMany(Comment, {as: 'Comments'});
    Comment.belongsTo(user);

    user.getUsersBooks = async (id) => {
        const currentUser = await user.findOne({where: {id}});
        const result = await currentUser.getBooks();
        return result;
    };
    user.setUsersBooks = async (id, values) => {
        const currentUser = await user.findOne({where: {id}});
        const books = JSON.parse(values.readersBooksId);
        const result = await currentUser.setBooks(books);
        return result;
    };

    user.getUsersComments = async (id) => {
        const currentUser = await user.findOne({where: {id}});
        const result = await currentUser.getComments();
        return result;
    };
    user.setUsersComments = async (id, comment) => {
        const currentUser = await user.findOne({where: {id}});
        const commentsIdArr = await user.getUsersComments(id);
        const commentId = JSON.parse(comment);
        commentsIdArr.push(commentId);
        const result = await currentUser.setComments(commentsIdArr);
        return result;
    };

    user.getUsersCommentedBook = async (ids) => {
        const usersIdArr = JSON.parse(ids);
        const result = await user.findAll({
            where: {
                id: {
                    [Op.or]: usersIdArr
                }},
            attributes: ['id', 'firstName']
        });
        result.forEach((commentedUser) => {
            commentedUser.dataValues.value = commentedUser.dataValues.firstName;
        });
        return result;
    };

    user.setOrderedBooks = async (id, values) => {
        const result = await user.update({orderedBooks: values.orderedBooks}, {
            where: {
                id
            }
        });
        return result;
    };
    user.getOrderedBooks = async (id) => {
        const currentUser = await user.findOne({where: {id}});
        return currentUser.orderedBooks;
    };
    user.getReaders = async () => {
        const result = await user.findAll({where: {role: 'reader'}});
        return result;
    };
    user.getReadersWithOrders = async () => {
        const result = await user.findAll({
            where: {role: 'reader',
                orderedBooks: {
                    [Op.not]: ''
                }}
        });
        return result;
    };
    user.likeBook = async (id, book) => {
        const currentUser = await user.findOne({where: {id}});
        const result = await user.update({likedBooks: `${currentUser.dataValues.likedBooks} ${book}`}, {
            where: {
                id
            }
        });
        return result;
    };

    return user;
};
