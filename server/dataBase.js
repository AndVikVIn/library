const Sequelize = require('sequelize');

const sequelize = new Sequelize('library', 'root', 'password', {
    dialect: 'mysql'
});

sequelize.sync();

module.exports = sequelize;
